package edu.uoc.android.currentweek;

import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;

import edu.uoc.android.currentweek.utilities.CalendarUtility;

import static org.junit.Assert.*;

public class CalendarUtilityTest {

    Integer expectedWeekNumber;
    Calendar calendar;

    @Before
    public void setUp() throws Exception {
        calendar = Calendar.getInstance();
        expectedWeekNumber = calendar.get(Calendar.WEEK_OF_YEAR);
    }

    @Test
    public void isTheCurrentWeekNumber() throws Exception {
        CalendarUtility calendarUtility = new CalendarUtility();

        // Test with the expected week number
        assertTrue(calendarUtility.checkCurrentWeekNumber(expectedWeekNumber));

        // Test with wrong week numbers (above and below)
        assertFalse(calendarUtility.checkCurrentWeekNumber(expectedWeekNumber-1));
        assertFalse(calendarUtility.checkCurrentWeekNumber(expectedWeekNumber+1));

        // Test with values outside the existing range
        assertFalse(calendarUtility.checkCurrentWeekNumber(0));
        assertFalse(calendarUtility.checkCurrentWeekNumber(54));
    }

}
